package pl.sunnyslab.spring.springtest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AccountController {

	@RequestMapping("/")
	public String getHome(Model model) {
		model.addAttribute("greet", "kurwa dzialam");
		return "accounts";
	}
}
